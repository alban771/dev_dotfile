#! /usr/bin/env python
"""
Scripts that take one or several youtube channel name and print the last online video info. 
"""

from requests import Session
import json
import sys




def channel_newest_video_info( channel ):
    url = 'https://www.youtube.com/user/{}/videos?view=0&sort=dd&flow=grid'.format(channel)
    r = Session().get( url, 
            params={'view':0,'sort':'dd','flow':'grid'},
            cookies={
                'CONSENT':'YES+cb.20211026-09-p1.en+F+072',
#                'PREF':'tz=UTC',
#                'VISITOR_INFO1_LIVE':'R17OTGZ3MHU',
#                'YSC':'mD1vBKklSpE; GPS=1',
                }
            )
    r.raise_for_status()
    start = r.text.index('gridVideoRenderer')+19
    a = 1
    i = 1
    for c in r.text[start+1:]:
        if c == '{': a+=1
        if c == '}': a-=1
        i+=1
        if a == 0: break
    return json.loads(r.text[start:start+i])


def report_info( info ): 
    print( """
{:^40}
{:<10}: {}
{:<10}: {}
{:<10}: {}
{:<10}: {}
        """.format(
        info['title']['runs'][0]['text'],
        "duration",info['thumbnailOverlays'][0]['thumbnailOverlayTimeStatusRenderer']['text']['simpleText'],
        "date", info['publishedTimeText']['simpleText'],
        "id", info['videoId'],
        "format",' '.join(['{width}x{height}'.format(**a) for a in info['thumbnail']['thumbnails']]),
        ))


if __name__ == '__main__':


    for channel in sys.argv[1:]:
        report_info( channel_newest_video_info( channel ) )







        

