#! /usr/bin/env python
import sys
from PyPDF2 import PdfFileReader, PdfFileWriter


def pdf_copy_last_page(input_file, output_stream):
    with open(input_file, 'rb') as input_stream:
        writer = PdfFileWriter()
        reader = PdfFileReader(input_stream)
        n = reader.getNumPages()
        print( "input pages #{}".format(n) )
        writer.addPage(reader.getPage(n-1))
        writer.write(output_stream)

    output_stream.close()

if __name__ == '__main__':
    pdf_copy_last_page( sys.argv[1], open('last_page_copy.pdf', 'wb'))
