#! /usr/bin/env python
import sys
try:
    from PyPDF2 import PdfFileReader, PdfFileWriter
except ImportError:
    from PyPdf import PdfFileReader, PdfFileWriter


#ref https://stackoverflow.com/questions/3444645/merge-pdf-files
def pdf_cat(input_files, output_stream):
    input_streams = []
    try:
        for input_file in input_files:
            input_streams.append(open(input_file, 'rb'))
        writer = PdfFileWriter()
        for reader in map(PdfFileReader, input_streams):
            for n in range(reader.getNumPages()):
                writer.addPage(reader.getPage(n))
        writer.write(output_stream)

    finally:
        for f in input_streams:
            f.close()
        output_stream.close()

if __name__ == '__main__':
    pdf_cat(sys.argv[1:], open('merged_by_python.pdf', 'wb'))

