"" Keep hands on usefull keys
""
nno <up> <Nop>
nno <down> <Nop>
nno <right> <Nop>
nno <left> <Nop>
ino <up> <Nop>
ino <down> <Nop>
ino <right> <Nop>
ino <left> <Nop>

"" Keep away from Ex-mode, or reach it with gQ
""
nmap Q <Nop>


"" See by default current windows full size
"" use the sequence to persist windows dimensions
""
noremap <C-S-k> <C-w>k50<C-w>_
noremap <C-S-j> <C-w>j50<C-w>_
noremap <C-S-h> <C-w>h50<C-w>\|
noremap <C-S-l> <C-w>l50<C-w>\|
noremap <C-k> <C-w>k<C-w>_
noremap <C-j> <C-w>j<C-w>_
noremap <C-h> <C-w>h<C-w>\|
noremap <C-l> <C-w>l<C-w>\|

"" From terminal mode
""
if has('nvim')
  tnoremap <Esc>  <C-\><C-n>
  tnoremap <A-[>  <Esc>
  tnoremap <C-k>  <C-\><C-n><C-w>k<C-w>_
  tnoremap <C-j>  <C-\><C-n><C-w>j<C-w>_
  tnoremap <C-l>  <C-\><C-n><C-w>l<C-w>|
  tnoremap <C-h>  <C-\><C-n><C-w>h<C-w>|
  tnoremap <C-w>  <C-\><C-n>gt
  tnoremap <C-q>  <C-\><C-n>gT
endif

noremap <C-w> gt
noremap <C-q> gT

"nb  <c-w>v == vertical split
"nb  <c-w>s == horizontal split
"nb  <c-w>q == quit current window




"" Complete the opened punctuation before writing inside
"" or double the closing to intentionaly leave it empty
""
inoremap ()) ()
inoremap {}} {}
inoremap []] []
inoremap <>> <>
inoremap () ()<ESC>i
inoremap {} {}<ESC>i
inoremap [] []<ESC>i
inoremap <> <><ESC>i


"" Buffer back and forth
"" waiting for unimpair integration
""
map [<space>     :bn<cr>
map ]<space>     :bp<cr>
vmap ]<space>    <Esc>:bn<cr>
vmap [<space>    <Esc>:bp<cr>


"" Custom clip
""
vmap <c-y>   !xclip; xclip -o<cr>
vmap <c-p>   <esc>:r !xclip -o<cr>v
if has('nvim')
  " cool expr binding to mimic <C-R> paste in terminal
  tnoremap <expr> <C-R> '<C-\><C-N>"'.nr2char(getchar()).'pi'
endif


" leader is a first parameterized key
"
let mapleader = "!" " default to \\
let mapleader = "\\" " has meaning only when mapping is defined


set runtimepath^=~/.vim
if &compatible | set nocp | endif " may have side effect
set number
set nohlsearch
set path+=**
set wildmenu
set sw=4 ts=4 sts=4 expandtab
set foldmethod=indent
set scrolljump=5
set scrolloff=5  " number of lines to keep below/above the cursor.
set inccommand=nosplit "split may show redundant match with current window
set tag+=./.tags,.tags
set ruler
set rulerformat=%30(%5.l:%-3c\ %2p%%\ \ \b\u:%-2n\ \f\t:%Y%)
set textwidth=100
set formatoptions+=tcr1
set colorcolumn=+1
set winminwidth=0
set winminheight=0
set noequalalways
set equalprg=clang-format



" file type
"
autocmd! BufNewFile,BufRead *.sls set ft=yaml  " saltstack configuration
autocmd! BufNewFile,BufRead *.har set ft=json


" theme
"
colorscheme desert
hi ColorColumn  ctermfg=NONE ctermbg=236 cterm=NONE
hi VertSplit    ctermfg=250 ctermbg=0  cterm=NONE
if has('nvim')
  hi! TermCursorNC ctermfg=NONE ctermbg=249 cterm=NONE
endif

" Show end of line spaces...
match ForbiddenWhiteSpace /\s\+$/
" ... but not while typing
autocmd! InsertEnter * match ForbiddenWhiteSpace /\s\+\%#\@<!$/
hi ForbiddenWhiteSpace ctermfg=NONE ctermbg=179


" Global variables
"
let g:vimdir = $XDG_CONFIG_HOME . '/nvim'
let g:python_host_prog ='/usr/bin/python2'
let g:python3_host_prog='/usr/bin/python3'
let g:loaded_node_provider = 0
let g:loaded_ruby_provider = 0
let g:loaded_perl_provider = 0

" Load addons
"
exec 'source ' . g:vimdir . '/packages.vim'

" Load ftplugins
"
syntax on


" Live config editing
"
autocmd! bufwritepost *.vim source $MYVIMRC


" Open main configuration files
"
function! EditVimConfigurationsCommand() abort
    let l:packages = g:vimdir.'/packages.vim'
    let l:ftplugin = g:vimdir.'/ftplugin/'.&filetype.'.vim'
    let l:cmd = '200vs ' . $MYVIMRC
    let l:cmd = join(['vs', '+'.escape( l:cmd, '\ ' ), l:packages])
    let l:cmd = join(['$tabe', '+'.escape( l:cmd, '\ '), l:ftplugin])
    exe l:cmd
endfunction

map <a-v> :call EditVimConfigurationsCommand()<cr>


" Toggle comment on line
"
function! ToggleComment() abort
  let l:txt = getline('.')
  let l:pos = match(l:txt, '^\b*'.b:com_line)
  if l:pos == -1
    let l:txt = b:com_line . l:txt
  else
    let l:txt = l:txt[l:pos+2:]
  endif
  call setline(line('.'), l:txt )
endfunction

map <C-c> :call ToggleComment()<cr>

" In-file configuration
"
function! VimTopCommand() abort
  let l:i = 1
  let l:pos = match(getline(l:i), 'vim:')
  while l:pos != -1
    exec getline(l:i)[l:pos+4:]
    let l:i += 1
    let l:pos = match(getline(l:i), 'vim:')
  endwhile
endfunction

autocmd! BufNewFile,BufRead * call VimTopCommand()


" Show syntax highlighting groups for word under cursor
"
nmap <C-S-P> :call <SID>SynStack()<CR>
function! <SID>SynStack()
  if !exists("*synstack")
    return
  endif
  echo map(synstack(line('.'), col('.')), 'synIDattr(v:val, "name")')
endfunction









" Eg from Drew Neil
"
"nnoremap <silent> <Plug>TransposeCharacters xp
"            \:call repeat#set("\<Plug>TransposeCharacters")<CR>
"nmap cp <Plug>TransposeCharacters
"nn Q @='n.'<CR>
"nn Q :normal n.<CR>
