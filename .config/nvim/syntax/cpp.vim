"if exists("b:custom_syntax")
"  finish
"endif



syn keyword algorithm1          for_each for_each_n count count_if mismatch find find_if find_if_not find_end find_first_of adjacent_find search search_n copy
syn keyword algorithm2          copy copy_if copy_n copy_backward fill fill_n transform generate generate_n remove remove_if remove_copy remove_copy_if replace replace_if replace_copy replace_copy_if reverse reverse_copy rotate rotate_copy shift_left shift_right shuffle unique unique_copy
syn keyword algorithm3          lower_bound upper_bound equal_range


syn match customType	            display "\<\w\+_t\>" 


hi def link       algorithm1     Statement
hi def link       algorithm2     Statement
hi def link       algorithm3     Statement
hi def link       customType     Type

hi! customType ctermfg=79 ctermbg=NONE cterm=NONE


"let b:custom_syntax = "scad"
