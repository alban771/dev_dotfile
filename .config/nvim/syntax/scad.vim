" Custom vim file 
" Language: OpenSCAD
" Last Change:	2020 Sep 27
" ref: https://github.com/sirtaj/vim-openscad/blob/master/syntax/openscad.vim


if exists("b:current_syntax")
  finish
endif


syn keyword scadStructure           function module nextgroup=scadStructureId skipwhite skipempty
syn match scadStructureId           /\<\w*\>/ contained display


syn keyword scadStatement           echo let assign assert 
syn keyword scadConditional         if else  
syn keyword scadBoolean             true false
syn keyword scadRepeat              for intersection_for
syn keyword scadInclude             include use

syn keyword scadOperator            intersection union difference 
syn keyword scadOperator            scale rotate translate hull offset minkovski mirror multmatrix linear_extrude rotate_extrude 

syn keyword scadPrimitive         square circle polygon text import_dfx 
syn keyword scadPrimitive         cube sphere cylindre polyhedron surface 

syn keyword scadBuiltin             abs acos asin atan atan2 ceil cos exp floor ln log 
syn keyword scadBuiltin             lookup max min pow rands round sign sin sqrt tan
syn keyword scadBuiltin             str len search version version_num concat chr
syn keyword scadBuiltin             dxf_cross dxf_dim


syn match scadNumber	            display "\<\(-\)\?\d\+\(\.\d\+\)\?\>" 
syn match scadSpecialVariable	    display "\$[a-zA-Z]\+\>"  

syn region  scadComment             start="//" end="$"
syn region  scadString              start=/"/  skip=/\\"/ end=/"/


hi def link       scadStructure     Structure      
hi def link       scadStructureId   Identifier      
hi def link       scadConditional   scadStatement
hi def link       scadRepeat        scadStatement
hi def link       scadLabel         Label
hi def link       scadStatement     Statement
hi def link       scadBoolean       Boolean
hi def link       scadOperator      Operator
hi def link       scadNumber        Number
hi def link       scadComment       Comment
hi def link       scadBuiltin       Function
hi def link       scadSpecialVariable       Special
hi def link       scadInclude       Include
hi def link       scadPrimitive     Keyword








let b:current_syntax = "scad"

