
set equalprg='clang-format'

hi ForbiddenWhiteSpace ctermfg=NONE ctermbg=179

match ForbiddenWhiteSpace /\s\+$/
" Do not highlight space while typing
autocmd! InsertEnter * match ForbiddenWhiteSpace /\s\+\%#\@<!$/


set tag=.tags,
set tag+=../externals/tags
set tag+=../../submodules/*/tags


nnoremap gh :e **/%:t:r.h*<CR>
nnoremap gi :e **/%:t:r.i*<CR>
nnoremap gc :e **/%:t:r.c*<CR>
