setlocal tag+=/usr/lib/python3.9/tags
setlocal tag+=~/.local/lib/python3.9/tags

let b:com_line='#'

""  Reduces expand time lookup by dedicating a directory to cpp
""  rather than UltiSnip lookup on snip names prefixes 
let b:UltiSnipsSnippetDirectories = [ $HOME.'.vim/UltiSnips/python' ]
