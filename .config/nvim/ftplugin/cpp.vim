let b:com_line = '//'
setlocal sw=2 ts=2 sts=2 expandtab
setlocal textwidth=80
setlocal formatoptions+=tc

set tag+=/usr/include/c++/*/tags
set tag+=/usr/include/boost/tags


if has('nvim')
    exec 'source ' . g:vimdir . '/ftplugin/cpp.lua'
endif


"" Idopte pres filters
""
vmap <C-S-s>   !~/.local/filter sh_eval<cr>
vmap <C-S-c>   !~/.local/filter c_preproc<cr>


"" Netatmo to header or implem
""
nnoremap <expr> gh :e **/%:t:r.h*
nnoremap <expr> gi :e **/%:t:r.c*


""  Reduces expand time lookup by dedicating a directory to cpp
""  rather than UltiSnip lookup on snip names prefixes
""
"if exists('g:UltiSnipsEditSplit')
"    let b:UltiSnipsSnippetDirectories = [ g:vimdir . '/ultisnips/cpp' ]
"endif


"function! GetWord( mark ) abort
"  let l:line = getline(a:mark)
"  let l:pre = strcharpart(l:line, 0, match(l:line, '\W\+', col(a:mark)))
"  return split(l:pre, '\W\+')[-1]
"endfunction
"
"
"function! GotoHeader() abort
"  let l:pattern = '**/' . expand('%:t:r')
"  let l:ext = filter( ['.hpp', '.h'], 'filewritable( l:pattern . v:val )' )
"  if len( l:ext ) > 0
"    :edit +/GetWord('.') l:pattern . l.ext[0]
"  else
"    :echo 'No header file found: ' . l:pattern . '.h{,pp}'
"  endif
"endfunction
"
"
"function! GotoImplementation() abort
"  let l:pattern = '**/' . expand('%:t:r')
"  let l:ext = filter( ['.cpp', '.ipp', '.c'], 'filewritable( l:pattern . v:val )' )
"  if len( l:ext ) > 0
"    :edit +/GetWord('.') l:pattern . l.ext[0]
"  else
"    :echo 'No implementation file found: ' . l:pattern . '.{cpp,ipp,c}'
"  endif
"endfunction


"" Boost snippet extractor
""

"function! MyDecodeUrl() abort
"        %s/&lt;/</g
"        %s/&gt;/>/g
"        %s/&quot;/"/g
"        %s/&amp;/\&/g
"endfunction
"
"
"function! ExtractBoostSnippet() abort
"        echo 'extract boost sinppet from html'
"        call search( '^#include')
"        call deletebufline( bufname(), 1, search( '^\s*<', 'bn' ))
"        call deletebufline( bufname(), search( '^\s*<', 'n'), line('$'))
"        call MyDecodeUrl()
"        %s/<a[^>]*>//g
"        %s/<\/a>//g
"        1
"endfunction
"
"au BufEnter *.cpp if getline(1) =~ '<!DOCTYPE' | call ExtractBoostSnippet() | endif

