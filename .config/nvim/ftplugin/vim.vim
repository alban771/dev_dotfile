packadd vint
packadd vim-scriptease

setlocal sw=4 ts=4 sts=4 expandtab
setlocal textwidth=100

let b:com_line = '"'
let g:syntastic_vim_checkers = ['vint'] 
