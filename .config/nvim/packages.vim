" Expect Vim 8.0 or Neovim
""  Package manager minpac makes use of vim8 package management features
""  It automates the fetch in ~/.vim/pack/minpac/{start,opt}/ and generates
""  the doc with :helptags ~/.vim/pack. The rtp, and the packadd command are built in.


let s:file = g:vimdir . '/packages.vim'

function! PackInit() abort

    packadd minpac

    "  Clone the repo of minpac if not found
    "
    if !exists('g:loaded_minpac')
        let l:git = system('which git')
        if v:shell_error
            return
        endif
        let l:minpacdir = g:vimdir . '/pack/minpac/opt/minpac'
        if isdirectory(l:minpacdir)
            exe '!rm -rf' l:minpacdir
        endif
        exe '!git clone https://github.com/k-takata/minpac' l:minpacdir
        packadd minpac
    endif

    " List plugins for fetch, update or clean
    "
    call minpac#init()
    call minpac#add('k-takata/minpac',{'type':'opt'})
    call minpac#add('ms-jpq/chadtree', {'type':'opt', 'branch':'chad',
        \'do':'python -m chadtree deps'})
    call minpac#add('scrooloose/nerdtree', {'type':'opt'})
    call minpac#add('machakann/vim-highlightedyank')
    call minpac#add('tpope/vim-surround')
    call minpac#add('tpope/vim-repeat')
    call minpac#add('SirVer/ultisnips')
    call minpac#add('tommcdo/vim-exchange')
    "call minpac#add('scrooloose/syntastic')
    "call minpac#add('sjl/gundo.vim')
    call minpac#add('junegunn/vader.vim')
    call minpac#add('junegunn/vim-peekaboo')
    call minpac#add('christianrondeau/vim-base64')
    call minpac#add('alx741/vinfo', {'type': 'opt'})            " Info
    call minpac#add('kuniwak/vint', {'type':'opt'})             " VimL lint
    call minpac#add('tpope/vim-scriptease', {'type':'opt'})     " VimL
"    call minpac#add('honza/vim-snippets', {'type':'opt'})      " VimL
    call minpac#add('r0mai/vim-djinni', {'type':'opt'})         " Djinni syntax
    call minpac#add('udalov/kotlin-vim', {'type':'opt'})        " Kotlin syntax
    call minpac#add('neovim/nvim-lspconfig')


endfunction

command! PackUpdate exe 'source ' s:file |
        \call PackInit() | call minpac#update() | packloadall! |
        \exe 'source ' s:file

command! PackClean call PackInit() | call minpac#clean()


" load plugins once
"
packloadall
"filetype plugin indent on


" Choose optionals
"
"  packadd chadtree
packadd nerdtree



"  usage: `vim +VinfoInit` as default shell info viewer
"
function! VinfoInit(query) abort
    packadd vinfo
    let b:skip_NERDTree = 1
    autocmd! BufRead vinfo_doc_repo/* set nonu | %foldo!
    call vinfo#load_doc( a:query )
endfunction



if exists('g:UltiSnipsEditSplit')
    let g:UltiSnipsEditSplit='tabdo'
    let g:UltiSnipsSnippetDirectories=[ g:vimdir . '/ultisnips'] " add third party snippets here
    let g:UltiSnipsSnippetStorageDirectoryForUltiSnipsEdit = g:vimdir .'/ultisnips'
"    let g:UltiSnipsExpandTrigger='<tab>'
"    let g:UltiSnipsListSnippets='<a-l>'
"    let g:UltiSnipsJumpForwardTrigger='<a-j>'
"    let g:UltiSnipsJumpBackwardTrigger='<a-k>'
    map <a-e> :UltiSnipsEdit<CR>
endif



autocmd! StdinReadPre * let s:std_in=1

if exists('NERDTreeCreator')

  " start nerdtree if no file were specified
  "
  autocmd VimEnter * if argc() == 0 && !exists("s:std_in") && !exists("b:skip_NERDTree") |
    \NERDTree | endif
  autocmd VimEnter * if argc() == 1 && isdirectory(argv()[0]) && !exists("s:std_in") |
    \exe 'NERDTree' argv()[0]  | wincmd p | ene | exe 'cd'.argv()[0] | endif

  " close vim if nerdtree is only window left open
  "
  autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTree") && b:NERDTree.isTabTree()) | q |
    \endif

  map <C-n> :NERDTree<CR>

endif


if exists('*highlightedyank#on')
  hi! HighlightedyankRegion ctermbg=241 ctermfg=NONE
  let g:highlightedyank_highlight_duration = 400 "ms
endif

if exists('g:peekaboo_window')
"  let g:peekaboo_window = "let w=winwidth(0) - &textwidth - 5 | exe 'rightbelow '.w.'vnew'"
  let g:peekaboo_window = "rightbelow 20new"
endif



" vim-repeat
"
"nnoremap <silent> <Plug>TransposeCharacters xp
"            \:call repeat#set("\<Plug>TransposeCharacters")<CR>
"nmap cp <Plug>TransposeCharacters
"
"nn Q @='n.'<CR>
"nn Q :normal n.<CR>
