#include <stdio.h>
#include <sys/types.h>
#include <Rectangle.h>		/* Rectangle asn1 type */




int main( int ac, char **av) {
	char buf[1024];		/* Temporary buffer */
	Rectangle_t *rectangle = 0; /* type to encode. Rq this 0 seems impt */
	asn_dec_rval_t rval; 	/* decoder return value */
	FILE *fp;		/* input file handler 	*/
	size_t size;		/* number of bytes read */
	char *filename; 	/* input file name 	*/

	/* Require single file argument */
	if( ac < 2 ) {
		fprintf(stderr, "Usage: %s <file.ber>\n", av[0]);
		exit(1);
	} else {
		filename = av[1];
	}


	/* Open file input as binary read-only */
	fp = fopen(filename, "rb");
	if(!fp) {
		perror(filename);
		exit(1);
	}

	/* Read up to the buffer size */
	size = fread(buf, 1, sizeof(buf), fp);
	fclose(fp);
	if(!size) {
		fprintf(stderr, "%s: Empty or broken\n", filename);
		exit(1);
	}


	/* Decode the input buffer as Rectangle type */
	rval = ber_decode(0, &asn_DEF_Rectangle, (void**)&rectangle, buf, size);
	if(rval.code != RC_OK) {
		fprintf(stderr, "%s: Broken Rectangle encoding at byte %ld\n", filename, (long)rval.consumed);
		exit(1);
	}


	/* Print the decoded Rectangle type as XML */
	xer_fprint(stdout, &asn_DEF_Rectangle, rectangle);


	return 0; /* Decoding finished successfully */
}
