# The following lines were added by compinstall
zstyle :compinstall filename '/home/me/.zshrc'
autoload -Uz compinit
compinit

# End of lines added by compinstall



##
##  PRE startx
##

bindkey -v
sudo loadkeys us
unsetopt BEEP
export HISTFILE=$HOME/.hist.zsh



##
##  ZSH theming
##

autoload -Uz  promptinit
promptinit
export PROMPT='%#_  '
#prompt walters



##
## LOCAL config
##

[ -z $LANG ] && source /etc/profile.d/locale.sh

[ -f $HOME/.aliases ] && source ${HOME}/.aliases

[ -n ${DISPLAY} ] && [ -d ~/.screenlayout ] && \
    source ~/.screenlayout/${SCREEN_LAYOUT:-*.sh}

export PATH=$PATH:$HOME/.local/bin
export PATH=$PATH:$HOME/.local/script/zsh:$HOME/.local/script/python

[ -z ${TMUX} ] && tmux
